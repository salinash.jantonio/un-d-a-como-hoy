un día como hoy...

Se compone de una impresora de tickets que se encuentra vinculada con las entradas de efemérides de Wikipedia, uno de los más grandes proyectos de conocimiento colectivo en la historia.

Cuando se presiona el botón rojo, un algoritmo elige de forma aleatoria entre una lista de acontecimientos que sucedieron un día como hoy, plasmándolo en un pedazo de papel.

Este dispositivo muestra una forma sencilla de explorar el contenido de Wikipedia haciendo uso de un simple botón.