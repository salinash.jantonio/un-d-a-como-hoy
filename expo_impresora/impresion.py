#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wikipedia
import random
import datetime
import time
import RPi.GPIO as GPIO
import threading
import os

global acon
global work_button

def get_info(channel):
	global acon
	global work_button
	if work_button:
		print "Dato"
		work_button = False
		r_acon = random.choice(acon).encode('utf-8')
		r_acon = get_year(r_acon)
		r_acon = get_spanish_day() + ("\n" * 2) + r_acon + ("\n" * 2) + 'www.wikipedia.org'
 		r_acon = resize_text(r_acon, 28)
		print r_acon
		file = open('aux.txt', 'w')
                file.write(r_acon)
                file.close()
		os.system('lp -d EPSON_TM-T20II aux.txt &')
		time.sleep(5)
		work_button = True
	else:
		print 'Boton Desactivado'

def get_year(text):
	texto = ''
	split_text = text.split(':')
	year = split_text[0]
	for i in range(1, len(split_text)):
		texto = texto + split_text[i]
	texto = "Un dÃ­a como hoy de " + year + texto
	return texto

def resize_text(text, n_col):
	text_aux = ''
	text_end = ''
	split_text = text.split(' ')

	for i in range(len(split_text)):
		aux = text_aux + split_text[i] + ' '
		if (len(aux) < n_col):
			text_aux = aux
		else:
			text_end = text_end + text_aux + '\n'
			text_aux = split_text[i] + ' '
	text_end = text_end + text_aux + '\n'
	return text_end

def find_between( s, first, last ):
    	try:
        	start = s.index( first ) + len( first )
        	end = s.index( last, start )
        	return s[start:end]
    	except ValueError:
        	return

def get_spanish_day():
	fecha = datetime.datetime.today()
	dia = fecha.day
	n_mes = fecha.month
	if n_mes == 1:
		mes = "enero"
	elif n_mes == 2:
		mes = "febrero"
	elif n_mes == 3:
		mes = "marzo"
	elif n_mes == 4:
		mes = "abril"
	elif n_mes == 5:
    		mes = "mayo"
    	elif n_mes == 6:
		mes = "junio"
	elif n_mes == 7:
		mes = "julio"
	elif n_mes == 8:
		mes = "agosto"
	elif n_mes == 9:
		mes = "septiembre"
	elif n_mes == 10:
		mes = "octubre"
	elif n_mes == 11:
		mes = "noviembre"
	elif n_mes == 12:
		mes = "diciembre"

	return str(dia) + " de " + mes

def get_acon(dia):
	list_acon = []
	wikipedia.set_lang("es")
	info_dia = wikipedia.page(dia)
	cont_dia = info_dia.content
	acon_dia = find_between(cont_dia, "== Acontecimientos ==", "== Nacimientos ==")
	acon_dia = acon_dia.split('\n')
	l_acon_dia = len(acon_dia)
	for i in range(1, l_acon_dia):
		if acon_dia[i] != '':
			list_acon.append(acon_dia[i])
	return list_acon

time.sleep(20)
dia = ''
work_button = False
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(5, GPIO.IN, pull_up_down=GPIO.PUD_UP)

if __name__ == '__main__':
	try:
		GPIO.add_event_detect(5, GPIO.FALLING,callback=get_info,bouncetime=10000)
		while 1:
			aux_dia = get_spanish_day()
			if dia != aux_dia:
				dia = aux_dia
				work_button = False
				print 'Descarga de Info'
				acon = get_acon(dia)
				work_button = True
				print 'Info Lista'
			time.sleep(1)
	except Exception as e:
		print e
